from keras.models import load_model
import tensorflow as tf
from keras import backend as K
import numpy as np
from PIL import Image
from skimage.io import imshow, imread
from skimage.transform import resize
import matplotlib.pyplot as plt
import matplotlib.cm as cm

def mean_iou(y_true, y_pred):
    prec = []
    for t in np.arange(0.5, 1.0, 0.05):
        y_pred_ = tf.to_int32(y_pred > t)
        score, up_opt = tf.metrics.mean_iou(y_true, y_pred_, 2)
        K.get_session().run(tf.local_variables_initializer())
        with tf.control_dependencies([up_opt]):
            score = tf.identity(score)
        prec.append(score)
    return K.mean(K.stack(prec), axis=0)

def preprocess_image(image_path, IMG_CHANNELS=3, IMG_HEIGHT=128, IMG_WIDTH=128):

    img = imread(image_path)[:,:,:IMG_CHANNELS]
    img = resize(img, (IMG_HEIGHT, IMG_WIDTH), mode='constant', preserve_range=True)
    return img

def pred_image(model, input_img, out_path="pred_out.png"):

    prediction = model.predict(np.array([input_img]), verbose=1)
   
    prediction = (prediction > 0.5).astype(np.uint8)

    img_array = np.squeeze(prediction)

    plt.imsave(out_path, img_array, cmap=cm.gray)

if __name__ == "__main__":

    model_path = "models/trained_model.h5"
    image_path = "test.png"

    # preprocess the image
    img = preprocess_image(image_path)

    # load the model
    model = load_model(model_path, custom_objects={'mean_iou': mean_iou})

    pred_image(model, img)

