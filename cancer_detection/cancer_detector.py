#!/usr/bin/env python
# coding: utf-8

# In[1]:


#get_ipython().run_line_magic('matplotlib', 'inline')


# In[3]:


import numpy as np 
import pandas as pd 
from glob import glob 
from skimage.io import imread
import os
import shutil
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, auc, roc_auc_score
from sklearn.model_selection import train_test_split
from keras.preprocessing.image import ImageDataGenerator
from keras.applications.nasnet import NASNetMobile
from keras.applications.xception import Xception
from keras.utils.vis_utils import plot_model
from keras.layers import Dropout, Flatten, Dense, GlobalAveragePooling2D, Average, Input, Concatenate, GlobalMaxPooling2D
from keras.models import Model
from keras.callbacks import CSVLogger, ModelCheckpoint
from keras.optimizers import Adam
from livelossplot import PlotLossesKeras
print("Module import successful\n")

# In[4]:


# Output files
TRAINING_LOGS_FILE = "training_logs.csv"
MODEL_SUMMARY_FILE = "model_summary.txt"
MODEL_PLOT_FILE = "model_plot.png"
MODEL_FILE = "model.h5"
TRAINING_PLOT_FILE = "training.png"
VALIDATION_PLOT_FILE = "validation.png"
ROC_PLOT_FILE = "roc.png"
KAGGLE_SUBMISSION_FILE = "kaggle_submission.csv"


# In[5]:


# Hyperparams
SAMPLE_COUNT = 85000
TRAINING_RATIO = 0.9
IMAGE_SIZE = 224
EPOCHS = 1
BATCH_SIZE = 2
VERBOSITY = 1
TESTING_BATCH_SIZE = 50


# In[6]:


# Data (https://www.kaggle.com/c/histopathologic-cancer-detection/data)
input_dir = '../data/crt_ml_challenge_1/'
training_dir = input_dir + 'train/'
#data_frame = pd.DataFrame({'path': glob(os.path.join(training_dir,'*.tif'))})
#data_frame['id'] = data_frame.path.map(lambda x: x.split('train/')[1].split('.')[0])
#labels = pd.read_csv(input_dir + 'train_labels.csv')
#data_frame = data_frame.merge(labels, on='id')
#negatives = data_frame[data_frame.label == 0].sample(SAMPLE_COUNT)
#positives = data_frame[data_frame.label == 1].sample(SAMPLE_COUNT)
#data_frame = pd.concat([negatives, positives]).reset_index()
#data_frame = data_frame[['path', 'id', 'label']]
#print("Data_Frame imported. Next imread\n")
#data_frame['image'] = data_frame['path'].map(imread)
#print("Imread successful\n")


# In[7]:


training_path = '../data/training'
validation_path = '../data/validation'

#for folder in [training_path, validation_path]:
#    for subfolder in ['0', '1']:
#        path = os.path.join(folder, subfolder)
#        os.makedirs(path, exist_ok=True)


# In[8]:


#training, validation = train_test_split(data_frame, train_size=TRAINING_RATIO, stratify=data_frame['label'])

#data_frame.set_index('id', inplace=True)

#for images_and_path in [(training, training_path), (validation, validation_path)]:
#    images = images_and_path[0]
#    path = images_and_path[1]
#    for image in images['id'].values:
#        file_name = image + '.tif'
#        label = str(data_frame.loc[image,'label'])
#        destination = os.path.join(path, label, file_name)
#        if not os.path.exists(destination):
#            source = os.path.join(input_dir + 'train', file_name)
#            shutil.copyfile(source, destination)

#print("Data storage - validation, training - succesful\n")
# In[9]:


# Data augmentation
print("data generation")

training_data_generator = ImageDataGenerator(rescale=1./255,
                                             horizontal_flip=True,
                                             vertical_flip=True,
                                             rotation_range=180,
                                             zoom_range=0.4, 
                                             width_shift_range=0.3,
                                             height_shift_range=0.3,
                                             shear_range=0.3,
                                             channel_shift_range=0.3)


# In[10]:


# Data generation
training_generator = training_data_generator.flow_from_directory(training_path,
                                                                 target_size=(IMAGE_SIZE,IMAGE_SIZE),
                                                                 batch_size=BATCH_SIZE,
                                                                 class_mode='binary')
validation_generator = ImageDataGenerator(rescale=1./255).flow_from_directory(validation_path,
                                                                              target_size=(IMAGE_SIZE,IMAGE_SIZE),
                                                                              batch_size=BATCH_SIZE,
                                                                              class_mode='binary')
testing_generator = ImageDataGenerator(rescale=1./255).flow_from_directory(validation_path,
                                                                           target_size=(IMAGE_SIZE,IMAGE_SIZE),
                                                                           batch_size=BATCH_SIZE,
                                                                           class_mode='binary',
                                                                           shuffle=False)


# In[11]:


# Model
input_shape = (IMAGE_SIZE, IMAGE_SIZE, 3)
inputs = Input(input_shape)

xception = Xception(include_top=False, input_shape=input_shape)(inputs)
nas_net = NASNetMobile(include_top=False, input_shape=input_shape)(inputs)

outputs = Concatenate(axis=-1)([GlobalAveragePooling2D()(xception), GlobalAveragePooling2D()(nas_net)])
outputs = Dropout(0.5)(outputs)
outputs = Dense(1, activation='sigmoid')(outputs)

model = Model(inputs, outputs)
model.compile(optimizer=Adam(lr=0.0001, decay=0.00001),
              loss='binary_crossentropy',
              metrics=['accuracy'])
model.summary()

#os.environ["PATH"] += os.pathsep + 'C:\Program Files (x86)\Graphviz2.38\\bin'
# In[13]:
#  Training
print("Model fit starts\n")

history = model.fit_generator(training_generator,
                              steps_per_epoch=len(training_generator), 
                              validation_data=validation_generator,
                              validation_steps=len(validation_generator),
                              epochs=EPOCHS,
                              verbose=VERBOSITY,
                              callbacks=[PlotLossesKeras(),
                                         ModelCheckpoint(MODEL_FILE,
                                                         monitor='val_acc',
                                                         verbose=VERBOSITY,
                                                         save_best_only=True,
                                                         mode='max'),
                                         CSVLogger(TRAINING_LOGS_FILE,
                                                   append=False,
                                                   separator=';')])


# In[14]:


# Training plots
epochs = [i for i in range(1, len(history.history['loss'])+1)]

print("Done!")

