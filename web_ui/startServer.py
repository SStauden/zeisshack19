from flask import Flask, render_template, request
import flask
import numpy as np
import cv2

app = Flask(__name__, template_folder='app/templates', static_folder='app/static')


@app.route('/')
def index():
    
    return render_template('index.html')

# @app.route('/sun')
# def sun_selected():
#     global current_method
#     current_method = "sun"
#     labels = read_label_file("/home/segnet/models/sun/sun.labels")
#     return render_template('index.html', current_method = current_method, data_labels = labels)

# @app.route('/camvid')
# def camvid_selected():
#     global current_method
#     current_method = "camvid"
#     labels = read_label_file("/home/segnet/models/camvid11/camvid11.labels")
#     return render_template('index.html', current_method = current_method, data_labels = labels)

# @app.route('/coco')
# def coco_selected():
#     global current_method
#     current_method = "coco"
#     labels = read_label_file("/home/darknet/models/coco/coco.names")
    
#     return render_template('index.html', current_method = current_method, data_labels = labels)

# @app.route('/openimages')
# def open_imgs_selected():
#     global current_method
#     current_method = "openimages"
#     labels = read_label_file("/home/darknet/models/openimages/openimages.names")
    
#     return render_template('index.html', current_method = current_method, data_labels = labels)

# @app.route('/imagenet')
# def imgnet_selected():
#     global current_method
#     current_method = "imagenet"
#     labels = read_label_file("/home/darknet/models/imagenet/imagenet.shortnames.list")
#     return render_template('index.html', current_method = current_method, data_labels = labels)

# @app.route('/yolo9000')
# def yolo9000_selected():
#     global current_method
#     current_method = "yolo9000"
#     labels = read_label_file("/home/darknet/models/yolo9000/9k.names")
#     return render_template('index.html', current_method = current_method, data_labels = labels)

@app.route('/upload', methods=['POST'])
def upload_file():

    # read send image data

    print(request.files)

    filestr = request.files['image_upload'].read()

    # convert string data to numpy array
    npimg = np.frombuffer(filestr, np.uint8)

     # convert numpy array to image
    orig_img = cv2.imdecode(npimg, cv2.IMREAD_UNCHANGED )

    # TODO Call the API server

    if len(flask.request.form):
        # we are calling the cell detection 
        pass

    else:
        # we are calling the cancer detection
        pass


    return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=True)

    
